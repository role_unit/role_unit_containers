== role_unit_containers

These containers are used by role_unit, which is an ansible role unit testing framework.

They can be built localy using make. For instance, to build debian10 container locally:

----
make debian10
----
