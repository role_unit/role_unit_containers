FROM debian:stretch-slim

MAINTAINER n0vember <n0vember@half-9.net>

### A bit of environment

ENV container docker
ENV TERM xterm
ENV DEBIAN_FRONTEND noninteractive
ENV DIND_COMMIT 3b5fac462d21ca164b3778647420016315289034

### Copy files in place

COPY is_ready /usr/local/bin/is_ready

### All RUN in one layer to gain space

RUN echo "===== APT INSTALLATION AND CLEANUP =====" && \
    apt-get update && apt-get -y dist-upgrade && \
    apt-get install -y --no-install-recommends \
        systemd \
        dbus \
        iproute2 \
        git \
        sudo \
        vim \
        net-tools \
        curl \
        python \
        uuid \
        ca-certificates \
        jq \
        python-apt \
        procps \
    && \
    echo "===== SYSTEMD PRIVILEGED PROTECTION =====" && \
    ( \
      cd /lib/systemd/system/sysinit.target.wants/ && \
      rm -f $(ls | grep -v systemd-tmpfiles-setup.service) \
    ) && \
    rm -f /lib/systemd/system/multi-user.target.wants/* && \
    rm -f /etc/systemd/system/*.wants/* && \
    rm -f /lib/systemd/system/local-fs.target.wants/* && \
    rm -f /lib/systemd/system/sockets.target.wants/*udev* && \
    rm -f /lib/systemd/system/sockets.target.wants/*initctl* && \
    rm -f /lib/systemd/system/basic.target.wants/* &&\
    rm -f /lib/systemd/system/anaconda.target.wants/* &&\
    rm -f /usr/lib/tmpfiles.d/systemd-nologin.conf && \
    rm -f /lib/systemd/system/systemd*udev* && \
    rm -f /lib/systemd/system/getty.target && \
    echo "===== ENABLE DOCKER IN DOCKER =====" && \
    set -x && \
    addgroup --system dockremap && \
    adduser --system --group dockremap && \
    echo 'dockremap:165536:65536' >> /etc/subuid && \
    echo 'dockremap:165536:65536' >> /etc/subgid && \
    curl -k -o /usr/local/bin/dind "https://raw.githubusercontent.com/docker/docker/${DIND_COMMIT}/hack/dind" && \
    chmod +x /usr/local/bin/dind && \
    echo "rpaulson ALL=(root) NOPASSWD: ALL" > /etc/sudoers.d/rpaulson && \
    echo "===== ENABLE IS_READY SCRIPT =====" && \
    chmod +x /usr/local/bin/is_ready && \
    echo "===== SOME GENERAL SYSTEM CLEANUP =====" && \
    apt-get purge -y ca-certificates-java && \
    apt-get purge -y manpages manpages-dev && \
    apt-get purge -y make && \
    apt-get purge -y git && \
    apt-get autoremove -y && \
    apt-get autoclean -y && \
    apt-get clean -y && \
    rm -rf /usr/share/locale/* && \
    rm -rf /var/cache/debconf/* && \
    rm -rf /usr/share/doc/* && \
    echo "Removing this for systemd-tmpfiles-clean.timer service : rm -rf /tmp/* /var/tmp/*"

### expose Docker in docker port
EXPOSE 2375

### define working directory
WORKDIR /workdir

### define systemd compatible stop signal
STOPSIGNAL SIGRTMIN+3

### Start systemd, be a system
CMD ["/lib/systemd/systemd"]
